import React from 'react';
import './App.css';
import { Provider } from 'react-redux';
import Header from './components/Header';
import MovieSearch from './components/MovieSearch';
import MovieList from './components/MovieList';
import { PersistGate } from 'redux-persist/integration/react';

import { store, persistor } from './redux/store';

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <div className='App'>
          <Header />
          <div className='MovieSearch'>
            <MovieSearch />
            <MovieList />
          </div>
        </div>
      </PersistGate>
    </Provider>
  );
};

export default App;
