import React from 'react';
// import propTypes from 'prop-types';
// import { bindActionCreators } from 'redux';
import { useSelector, useDispatch } from 'react-redux';
import { deleteMovie } from '../redux/actions/movieActions';
import MovieCard from './MovieCard';

const MovieList = () => {
  // grab movies state from redux store by useSelector hook
  const { movies } = useSelector((state) => state.mvStore);

  const dispatch = useDispatch(); // useDispatch hook to dispatch actions

  // handle delete by id
  const handleDelete = (e) => dispatch(deleteMovie(e.target.id));

  const showSelectedMovies =
    movies && movies.length ? (
      movies.map((mv, i) => {
        return (
          <MovieCard
            key={i}
            id={mv.id}
            title={mv.title}
            budget={mv.budget}
            release_date={mv.release_date}
            handleDelete={handleDelete}
          />
        );
      })
    ) : (
      <h3>Search movies on search bar click from drop down list to add</h3>
    );

  return (
    <div className='movie-list'>
      <h2>Movie List:</h2>
      {showSelectedMovies}
    </div>
  );
};

export default MovieList;

// Old connect HOC
// might be easier to test react component instead of using redux hook

// MovieList.propTypes = {
//   deleteMovie: propTypes.func.isRequired,
//   movies: propTypes.array.isRequired,
//   newMv: propTypes.object,
// };

// const mapStateToProps = (state) => ({
//   movies: state.mvStore.movies,
//   newMv: state.mvStore.newMv,
// });

// const mapDispatchToProps = (dispatch) => {
//   return bindActionCreators({ deleteMovie }, dispatch);
// };

// export default connect(mapStateToProps, mapDispatchToProps)(MovieList);
