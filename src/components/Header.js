import React from 'react';
import { useSelector } from 'react-redux';

const Header = () => {
  // grab avgBudget state from redux store by useSelector hook
  const { avgBudget } = useSelector((state) => state.mvStore);

  return (
    <header className='App-header'>
      <h1>Average Movie Budget: {avgBudget}</h1>
    </header>
  );
};

export default Header;

// const mapStateToProps = (state) => ({
//   avgBudget: state.mvStore.avgBudget,
// });

// const mapDispatchToProps = (dispatch) => ({});

// export default connect(mapStateToProps, mapDispatchToProps)(Header);
