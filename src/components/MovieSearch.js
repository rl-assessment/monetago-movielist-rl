import React, { useState, useEffect, useRef } from 'react';
// import propTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { addMovie } from '../redux/actions/movieActions';
import NoResult from './NoResult';

const MovieSearch = () => {
  // local state to control search bar
  const [display, setDisplay] = useState(false);
  const [dropList, setDropList] = useState([]);
  const [search, setSearch] = useState('');
  const [allowQuery, setAllowQuery] = useState(false);
  const [inputErr, setInputErr] = useState(false);

  const dispatch = useDispatch(); // useDispatch hook to dispatch actions

  // handle mouse click outside of search box
  const wrapperRef = useRef(null);
  // handle mouse click outside of search box
  useEffect(() => {
    window.addEventListener('mousedown', handleClickOutside);
    return () => window.removeEventListener('mousedown', handleClickOutside);
  });
  // handle mouse click outside of search box
  const handleClickOutside = (event) => {
    const { current } = wrapperRef;
    if (current && !current.contains(event.target)) {
      setDisplay(false);
      setSearch('');
    }
  };

  // display drop down list onChange by search bar input
  useEffect(() => {
    const API_KEY = '41a6894ca93cb1c78657d9e799e164de';
    const INCLUDES = '&language=en-US&page=1&include_adult=false';
    let url = `https://api.themoviedb.org/3/search/movie?query=${search}&api_key=${API_KEY}${INCLUDES}`;

    const movieResult = []; // store query results
    if (search.length && allowQuery) {
      fetch(url)
        .then((res) => res.json())
        .then(({ results }) => {
          results.forEach((elem) => (elem.id ? movieResult.push(elem) : null));
          setDisplay(true); // turn on display drop down
        })
        .catch((err) => console.log(err));
      setDropList(movieResult);
    }
  }, [search, allowQuery]); // onChange trigger query

  const handleInputChange = (e) => {
    setAllowQuery(true);
    setDisplay(false); // turn off dropdown list
    setSearch(e.target.value); // set search bar value
    setInputErr(false);
  };

  const handleClickAddMovie = (e) => {
    setSearch(''); // setSearch(e.target.title);
    setAllowQuery(false); // avoid re-render after click
    setDisplay(false); // turn off dropdown list
    const clickResult = dispatch(addMovie(e.target.id)); // dispatch addMovie action by movie id
    clickResult.then((result) =>
      result !== undefined ? setInputErr(true) : null
    );
  };

  const showDropDown = dropList.map((movie, i) => (
    <div
      title={movie.title}
      id={movie.id}
      onClick={handleClickAddMovie}
      className='dropdownlists'
      key={i}
      tabIndex='0'
    >
      {movie.title}
    </div>
  ));

  return (
    <div ref={wrapperRef} className='flex-container flex-column pos-rel'>
      <input
        className='search-input'
        placeholder='Enter movie name to search..'
        type='text'
        name='search'
        onChange={handleInputChange}
        value={search}
      />

      {inputErr ? <NoResult /> : null}

      {display && <div className='divDropdown'>{showDropDown}</div>}
    </div>
  );
};

export default MovieSearch;

// MovieSearch.propTypes = {
//   addMovie: propTypes.func.isRequired,
// };

// export default connect(null, { addMovie })(MovieSearch);
