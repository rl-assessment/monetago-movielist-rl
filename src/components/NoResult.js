import React from 'react';

const NoResult = () => {
  return (
    <div className='no-result'>
      <h4>Can not find details for this movie, please try other movie name</h4>
    </div>
  );
};

export default NoResult;
