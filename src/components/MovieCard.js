import React from 'react';

// show selected movie title, budget and button to delete
const MovieCard = ({ id, title, budget, release_date, handleDelete }) => {
  return (
    <div className='movie'>
      <label htmlFor='title'>Movie Title: </label>
      <span id='title'>{title}</span>
      <p>
        <label htmlFor='budget'>Budget: </label>
        <span id='budget'>{budget}</span>
      </p>
      <p>
        <label htmlFor='release_date'>Release date: </label>
        <span id='release_date'>{release_date}</span>
      </p>
      <button type='button' id={id} onClick={handleDelete}>
        Delete
      </button>
    </div>
  );
};

export default MovieCard;
