import { ADD_MOVIE, DELETE_MOVIE } from '../actions/types';

const initialState = {
  movies: [],
  newMv: {},
  avgBudget: 0,
};

const movieReducer = (state = initialState, action) => {
  let len = state.movies.length;
  let mvlist;
  let totalBudget = 0;
  let avgAmount;

  switch (action.type) {
    case ADD_MOVIE:
      len = len + 1;

      // push the new movie to the movies list
      mvlist = state.movies;
      mvlist.push(action.payload);

      // calculate average budget
      totalBudget = mvlist.reduce((acc, curr) => acc + curr.budget, 0);
      avgAmount = Math.floor(totalBudget / len);
      return {
        ...state,
        movies: mvlist,
        newMv: action.payload,
        avgBudget: avgAmount > 0 ? avgAmount : 0,
      };
    case DELETE_MOVIE:
      len = len - 1;

      // delete movie from list by id with filter method
      mvlist = state.movies.filter(
        (mv) => Number(mv.id) !== Number(action.payload)
      );

      // re-calculate average budget
      totalBudget = mvlist.reduce((acc, curr) => acc + curr.budget, 0);
      avgAmount = Math.floor(totalBudget / len);
      return {
        ...state,
        movies: mvlist,
        // newMv: action.payload,
        avgBudget: avgAmount > 0 ? avgAmount : 0,
      };
    default:
      return state;
  }
};

export default movieReducer;
