import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import movieReducer from './movieReducer';

const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['mvStore'],
};

const rootReducer = combineReducers({ mvStore: movieReducer });

export default persistReducer(persistConfig, rootReducer);
