import { ADD_MOVIE, DELETE_MOVIE } from './types';

// having trouble to get movie detail, requested new API key and works
export const addMovie = (id) => async (dispatch) => {
  const API_KEY = 'c4f365596041415b57a48303b18f7e44';
  const URL = `https://api.themoviedb.org/3/movie/${id}?api_key=${API_KEY}`;
  const options = {
    method: 'GET',
    headers: { 'Content-type': 'application/json' },
  };
  try {
    const response = await fetch(URL, options);
    const data = await response.json();
    if (data.id !== undefined) {
      dispatch({ type: ADD_MOVIE, payload: data });
    } else {
      return { status_code: 34 };
    }
  } catch (err) {
    console.log(err);
  }
};

// deleteMovie action delete by event.target id
export const deleteMovie = (id) => (dispatch) => {
  dispatch({ type: DELETE_MOVIE, payload: id });
};
