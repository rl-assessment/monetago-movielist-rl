# Front End Engineer Take Home Assignment

## Table of Contents

1. Getting Started
2. Objective
3. Specifications
4. The Movie Database API v3
5. You May Use

## 1. Getting Started

To reduce the time required, this project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app) (_requires Node.js >= 8.10 and npm >= 5.6_)  
Using this boilerplate is optional. If you choose not to, please include README instructions on how to run the Web App locally.

In the root project directory, run:  
`npm install`  
`npm start`  
Runs the app in the development mode.  
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.  
You will also see any lint errors in the console.

## Objective

Create a simple Average Movie Budget Web Application with React Redux that has autocomplete search, allows a User to build a list of movies and calculate the average movie budget of that list.  
You will be using a free API from [themoviedb.org](https://www.themoviedb.org/?language=en-US).  
[The Movie Database API: Getting Started](https://developers.themoviedb.org/3/getting-started/introduction)  
**Time Constraint:** You should spend approximately 3 hours of work, with a 24 hour deadline.

## Specifications

**1. Requirements**

- Must use React.js and Redux
- Must run locally
- Styles: It should look presentable but we prefer you focus your time on Javascript/React.

**2. has a Search Bar**

- When a User types into the Search Bar it should provide a dropdown list of autocomplete query results from `themoviedb` search.
- A User can click one of the results from the dropdown and it will add the movie to a Movie List.

**3. has a Movie List**

- The Movie List should contain results selected from the Search Bar.
- Each movie should display basic data, the title of the movie, description, the budget and the release date.
- There should be a button to remove a Movie from the Movie List.
- Each movie has a `budget` value, the Movie List should calculate and display the average `budget` of all of the current movies inside the Movie List.

## The Movie Database API v3

**API Key:** `41a6894ca93cb1c78657d9e799e164de`  
These are the two `GET` endpoints that you will need to complete this task, however, you are not limited to these and may use any other endpoint you want.

- **Search Movies Endpoint** - https://developers.themoviedb.org/3/search/search-movies
  https://api.themoviedb.org/3/search/movie?api_key=<<api_key>>&language=en-US&page=1&include_adult=false

- **Get Movie Details Endpoint** - https://developers.themoviedb.org/3/movies/get-movie-details

https://api.themoviedb.org/3/search/movie?api_key=<<api_key>>&language=en-US&page=1&include_adult=false

## You May Use

- **Package Management** - You may use `yarn` or `npm`.
- **Node modules** - You may use any boilerplate/node package that you feel is fit.
- **CSS frameworks** - You may use your own styling or any CSS framework.

Feel free to contact us about any questions you may have. Goodluck!
